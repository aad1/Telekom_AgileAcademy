
# Book

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **Boolean** |  |  [optional]
**isbn** | **String** |  |  [optional]
**price** | **Double** |  |  [optional]
**title** | **String** |  |  [optional]



